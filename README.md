## Description

Example Cypress tests using the UI Automation Playground
("http://uitestingplayground.com/")

The UI Automation Playground has a number of different challenges for UI automation
tools, from difficult selectors to handling waits and interactive elements.

Because of this, running some of the tests may be slow.

### Getting Started

- Install dependencies with `npm i`
- To run tests through the Cypress Test Runner, run `npm run test-ui`
- To run tests through the terminal, run `npm run test`
- To lint test files, run `npm run lint`