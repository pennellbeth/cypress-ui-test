/// <reference types='cypress' />

describe('Intercept API call', () => {
  beforeEach(() => {
    cy.visit('/ajax')
  })

  it('Normally takes 15 seconds to await the AJAX response', () => {
    cy.get('#ajaxButton').click()
    cy.get('.bg-success', { timeout: 15500 })
      .should('have.text', 'Data loaded with AJAX get request.')
  })

  it('Can be intercepted to reduce the wait', () => {
    cy.intercept('GET', '/ajaxdata', 'Data loaded with AJAX get request.')
    cy.get('#ajaxButton').click()
    cy.get('.bg-success').should('have.text', 'Data loaded with AJAX get request.')
  })

  it('Can make the API call directly', () => {
    cy.request('GET', '/ajaxdata').then((response) => {
      expect(response.status).to.eql(200)
      expect(response.body).to.eql('Data loaded with AJAX get request.')
    })
  })
})
