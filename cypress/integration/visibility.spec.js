/// <reference types='cypress' />

describe('Selectors with visibility issues', () => {
  context('Horizontal and vertical scrolling required to reveal button', () => {
    it('Can be located and clicked', () => {
      // I feel like this shouldn't work, and yet even though contains scrolls
      // the element, an assertion of 'be.visible' fails
      cy.visit('/scrollbars')
      cy.contains('Hiding Button').should('not.be.visible')
      cy.get('#hidingButton').click()
        .should('be.visible')
    })
  })

  context('Overlapped element requires scroll to reveal', () => {
    it('Can be located and interacted with', () => {
      cy.visit('/overlapped')
      cy.get('#name').type('Foo')
      cy.get('#name').should('have.value', 'Foo')
    })
  })
})
