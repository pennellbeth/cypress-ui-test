/// <reference types='cypress' />

describe('Basic login', () => {
  // Simple username/password submission form that provides text response rather
  // than using page redirects
  beforeEach(() => {
    cy.visit('/sampleapp')
  })

  it('Has correct default page elements', () => {
    cy.get('#loginstatus').should('contain.text', 'User logged out.')
    cy.get('#login').should('have.text', 'Log In')
  })

  it('Permits valid login and log out', () => {
    // Testing two things vs having repeated steps
    cy.get('input[type="text"]').type('Foo')
    cy.get('input[type="password"]').type('pwd')
    cy.get('#login').click()
      .then((button) => {
        expect(button).to.have.text('Log Out')
      })
    cy.get('#loginstatus').should(($el) => {
      expect($el).to.have.attr('class', 'text-success')
      expect($el).to.contain('Welcome, Foo!')
    })

    cy.get('#login').click()
      .should('have.text', 'Log In')
    cy.get('#loginstatus').should('contain.text', 'User logged out.')
  })

  it('Does not permit incorrect password', () => {
    cy.get('input[type="text').type('Bar')
    cy.get('input[type="password').type('password')
    cy.get('#login').click()
    cy.get('#loginstatus').should(($el) => {
      expect($el).to.have.attr('class', 'text-danger')
      expect($el).to.contain('Invalid username/password')
    })
  })

  it('Does not permit empty username', () => {
    // Same error as incorrect password here
    cy.get('input[type="password').type('password')
    cy.get('#login').click()
    cy.get('#loginstatus').should(($el) => {
      expect($el).to.have.attr('class', 'text-danger')
      expect($el).to.contain('Invalid username/password')
    })
  })
})
