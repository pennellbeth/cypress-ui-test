/// <reference types='cypress' />

describe('Dealing with random elements in UI behaviour', () => {
  context('Progress bar with random speed increments', () => {
    // Progress bar which begins on 25%, and increases at random speeds when
    // the start button is clicked. The aim of the test is to stop as close to
    // 75% as possible, with result and duration values listed upon stop to
    // show the speed (duration) and accuracy (result diff to 75%).
    beforeEach(() => {
      cy.visit('/progressbar')
    })

    it('Has required elements and default starting values', () => {
      cy.get('.progress-bar').should('have.attr', 'aria-valuenow', '25')
      cy.get('#startButton').should('be.visible')
      cy.get('#stopButton').should('be.visible')
    })

    it('Can stop on a prescribed value - most restrictive', () => {
      // First test iteration using the exact values worked for slower test runs,
      // but couldn't handle the quicker ones where result != 0
      cy.get('#startButton').click()
      // Slow speeds exceed Cypress' 4s timeout, so have added a larger one manually
      cy.get('.progress-bar', { timeout: 30000 }).should('have.attr', 'aria-valuenow', '75')
      cy.get('#stopButton').click()
      cy.get('#result').should('have.contain', 'Result: 0')
    })

    it('Can stop on a prescribed value - most relaxed', { defaultCommandTimeout: 30000 }, () => {
      // Second iteration reduced flake at the cost of assertion accuracy
      cy.get('#startButton').click()
      // >= 75 for high speed loads which bypass target value before element was found
      cy.get('.progress-bar').invoke('attr', 'aria-valuenow').should((obj) => {
        expect(parseInt(obj)).to.be.at.least(75)
      })
      cy.get('#stopButton').click()
      cy.get('#result').invoke('text').then((text) => {
        return parseInt(text.match(/Result: (\d{1,2})/)[1])
      }).should('be.lessThan', 26)
    })

    it('Can stop on a prescribed value - linear tolerance', { defaultCommandTimeout: 30000 }, () => {
      // Final iteration most stable but also most complex assertions
      cy.get('#startButton').click()
      // >= 75 for high speed loads which bypass target value before element was found
      cy.get('.progress-bar').invoke('attr', 'aria-valuenow').should((obj) => {
        expect(parseInt(obj)).to.be.at.least(75)
      })
      cy.get('#stopButton').click()
      cy.get('#result').invoke('text').then((text) => {
        // The shorter the duration, the more tolerance that is required on the result.
        // So went with a fraction-based tolerance.
        const duration = parseInt(text.match(/^.*duration: (\d.*)/)[1])
        // 10 * 1000 as the duration is in milliseconds
        const bufferTime = 1 / duration * 10000
        cy.get('#result').invoke('text').then((text2) => {
          return parseInt(text2.match(/Result: (\d{1,2})/)[1])
        }).should('be.lessThan', bufferTime)
      })
    })
  })
})
