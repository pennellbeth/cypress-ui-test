/// <reference types='cypress' />

describe('Tricky selectors', () => {
  context('Dynamic unique attribute selector', () => {
    // Button has a unique 'id' attribute (a UUID) which changes upon page reload
    it('Can be selected using label text', () => {
      cy.visit('/dynamicid')
      cy.contains('Button with Dynamic ID').click()
        .should('be.visible')
        .and('have.attr', 'id')
    })

    it('Changes the ID attribute upon page reload', () => {
      cy.visit('/dynamicid')
      cy.contains('Button with Dynamic ID').invoke('attr', 'id')
        .then((id) => {
          const idBefore = id
          expect(id).to.match(
            /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/
          )
          cy.reload()
          cy.contains('Button with Dynamic ID').invoke('attr', 'id')
            .then((id) => {
              const idAfter = id
              expect(id).to.match(
                /^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/
              )
              expect(idBefore).to.not.eql(idAfter)
            })
        })
    })
  })

  context('Multiple class references in one element', () => {
    // 3 buttons on the page using similar classes and same text labels.
    // Classes have partially unique references
    it('Can be selected using the class', () => {
      cy.visit('/classattr')
      cy.get('.btn-primary').click()
      cy.on('window:alert', (text) => {
        // window:alert is the event which gets fired on alert open
        expect(text).to.eql('Primary button pressed')
      })
      cy.location().should((loc) => {
        expect(loc.pathname).to.eql('/classattr')
      })
    })
  })

  context('Element inner text not normalised', () => {
    it('Can be located with a text selector', () => {
      cy.visit('/verifytext')
      // Thankfully contains just handles this automatically
      cy.get('.bg-primary').contains('Welcome UserName!')
        .should('have.attr', 'class', 'badge-secondary')
    })
  })

  context('Dynamic table with no unique identifiers', () => {
    it('Can locate the correct row and cell combo', () => {
      // Table values, cells and rows are dynamic.
      // Table is based on divs with aria attributes
      cy.visit('/dynamictable')
      var targetHeader = 'CPU' // eslint-disable-line no-var
      var targetRow = 'Chrome' // eslint-disable-line no-var

      // Find the horizontal index of CPU column
      cy.contains('span[role="columnheader"]', targetHeader).invoke('index')
        .then((index) => {
          // Apply this index value to the Chrome row to get the desired cell
          cy.contains('span[role="cell"]', targetRow).siblings().eq(index - 1)
            .invoke('text')
            .then((text) => {
              const tableValue = text
              expect(tableValue).to.match(/\d{1,3}(.\d{1,2})?%/)
              // Get the answer text and compare it to the value we found
              cy.get('.bg-warning').invoke('text')
                .then((targetText) => {
                  expect(`${targetRow} ${targetHeader}: ${tableValue}`).to.eql(targetText)
                })
            })
        })
    })
  })
})
